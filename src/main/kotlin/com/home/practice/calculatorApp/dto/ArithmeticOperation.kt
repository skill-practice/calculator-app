package com.home.practice.calculatorApp.dto

enum class ArithmeticOperation(
        val key: String,
        val process: (num1: Int, num2: Int) -> Int
) {
    ADD("+", ::add),
    SUBTRACT("-",::subtract),
    MULTIPLY("*", ::multiply),
    DIVIDE("/", ::divide),

}

fun add(num1: Int, num2: Int): Int = num1 + num2
fun subtract(num1: Int, num2: Int): Int = num1 - num2
fun multiply(num1: Int, num2: Int): Int = num1 * num2
fun divide(num1: Int, num2: Int): Int = num1 / num2
